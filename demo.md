# Hubble UI
cilium hubble ui --port-forward=8081

# Open
`kubectl exec tiefighter -- curl -s -XPOST deathstar.default.svc.cluster.local/v1/request-landing` -> "Ship Landed"
`kubectl exec xwing -- curl -s -XPOST deathstar.default.svc.cluster.local/v1/request-landing` -> "Ship Landed"

# L3
`kubectl apply -f /vagrant/l3-policy.yaml`
`kubectl exec tiefighter -- curl -s -XPOST deathstar.default.svc.cluster.local/v1/request-landing` -> "Ship Landed"
`kubectl exec xwing -- curl -s -XPOST deathstar.default.svc.cluster.local/v1/request-landing` -> *crash*

# L7
`kubectl delete -f /vagrant/l3-policy.yaml`

*ff wachten*

`kubectl apply -f /vagrant/l7-policy.yaml`
`kubectl exec xwing -- curl -s -XPOST deathstar.default.svc.cluster.local/v1/request-landing` -> "Ship Landed"
`kubectl exec tiefighter -- curl -s -XPUT deathstar.default.svc.cluster.local/v1/exhaust-port` -> "Access Denied"